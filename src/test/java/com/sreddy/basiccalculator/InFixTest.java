package com.sreddy.basiccalculator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Provides the test cases for <code>InFix</code> class
 * 
 * @author Sudhakar Ankenapalli
 *
 */
public class InFixTest {

	private InFix inFix;
	
	
	@Before
	public void setUp() {
		inFix = new InFix();
	}
	
	@Test
	public void testAddition() {
		int sum = inFix.calc("5+2");
		
		assertTrue(7 == sum);
		
	}
	
	@Test
	public void testSubstraction() {
		int sum = inFix.calc("5-2");
		
		assertTrue(3 == sum);
		
	}
	
	@Test
	public void testMultiplication() {
		int sum = inFix.calc("5*2");
		
		assertTrue(10 == sum);
		
	}
	
	@Test
	public void testDivision() {
		int sum = inFix.calc("5/2");
		
		assertTrue(2 == sum);
		
	}
	
	@Test
	public void testAdditionAndMultiplication() {
		int sum = inFix.calc("4+5*2");
		
		assertTrue(14 == sum);
		
	}
	
	@Test
	public void testAdditionAndMultiplicationAndDivision() {
		int sum = inFix.calc("4+5*2+5/2");
		
		assertTrue(16 == sum);
		
	}
	
}
