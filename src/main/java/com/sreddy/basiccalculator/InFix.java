package com.sreddy.basiccalculator;

import java.util.Stack;
import java.util.StringTokenizer;

/**
 *  Class provides the basic calculator functionality 
 *  
 *  @author Sudhakar Ankenapalli
 *  
 */
public class InFix implements Calc {

	/**
	 * Parse the expression of string
	 * 
	 * @param expression
	 * 
	 * @return calculated final value
	 */
    public int calc(String expression) {
        expression = expression.replaceAll("[\t\n ]", "")+"=";
        String operator = "*/+-=";
        
        StringTokenizer tokenizer = new StringTokenizer(expression, operator, true);
        
        Stack<String> operatorStack = new Stack<String>();
        Stack<String> valueStack = new Stack<String>();
        
        while(tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            
            if(operator.indexOf(token)<0)
                valueStack.push(token);
            else
                operatorStack.push(token);
            
            resolve(valueStack, operatorStack);
        }
        
        String lastOne = (String)valueStack.pop();
        
        return Integer.parseInt(lastOne);   
    }

    /**
     * 
     * @param op is the operator
     * @return priority of the operator
     */
    protected int getPriority(String op) {
    	
        if(op.equals("*") || op.equals("/"))
            return 1;
        else if(op.equals("+") || op.equals("-"))
            return 2;
        else if(op.equals("="))
            return 3;
        else
            return Integer.MIN_VALUE;
    }

    /**
     * 
     * @param values
     * @param operators
     */
    protected void resolve(Stack<String> values, Stack<String> operators) {
    	
        while(operators.size() >= 2) {
        	
            String first = (String)operators.pop();
            String second = (String)operators.pop();
            
            if(getPriority(first)<getPriority(second)) {
                operators.push(second);
                operators.push(first);
                
                return;
            } else {
                String firstValue = (String)values.pop();
                String secondValue = (String)values.pop();
                
                values.push(getResults(secondValue, second, firstValue));
                operators.push(first);
            }
        }
    }

    /**
     * apply the operator on the operands
     * @param operand1
     * @param operator
     * @param operand2
     * @return
     */
    protected String getResults(String operand1, String operator, String operand2) {

        int op1 = Integer.parseInt(operand1);
        int op2 = Integer.parseInt(operand2);
        
        if(operator.equals("*"))
            return ""+(op1*op2);
        else if(operator.equals("/"))
            return ""+(op1/op2);
        else if(operator.equals("+"))
            return ""+(op1+op2);
        else if(operator.equals("-"))
            return ""+(op1-op2);
        else
            return null;
    }

    public static void main(String[] args) {
    	
    	if(args.length == 0) {
    		System.err.println("No argument provided, usage: java InFix <expression as 5-2+5/2>");
    		System.exit(1);
    	}
    	
    	Calc infix = new InFix();
        infix.calc(args[0]); 
        
        //String expression = "5/2+2";
        
        System.out.println(args[0]+"="+infix.calc(args[0]));
        
    }
}
