package com.sreddy.basiccalculator;

/**
 * Provides methods to implement the basic calculator functionality
 * 
 * @author sreddy
 *
 */
public interface Calc {

	/**
	 * Define calc computation
	 * 
	 * @param expression is the input in the form of simple math expression
	 * @return int 
	 */
	int calc(String expression);
	
}
