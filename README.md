
Please follow the instructions to build and run the app

Step 1:

This project is maven based so need to have maven installed if you do not have it already

Clone the project with following command and run subsequent commands

git@bitbucket.org:asreddy/basic-calculator.git

cd basic-calculator

mvn install

Step 2:

run the following command to run the basic calculator application on console

java -classpath target/basic-calculator-1.0.0-SNAPSHOT.jar com.sreddy.basiccalculator.InFix 2+5+5/2

